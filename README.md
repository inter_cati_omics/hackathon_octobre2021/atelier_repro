# Hackathon inter-CATIs Sète 2021: Atelier Reproductibilité 

lien vers le GITLAB PAGES : [https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/atelier_repro/](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/atelier_repro/)

{% pdf title="Slides présentation de l'atelier", src="HackathonReproIntro_20210930.pdf", width="100%", height="550", link=false %}{% endpdf %}


# Planning pour les 3 demies-journées du hackathon

-	J1.Containers [Tous ensemble]
-	J2.NextFlow
-	J2.SnakeMake
-	J3 Hands-On


# Détails des 3 demies-journées du hackathon

## J.1.Containers

Conda
-	intérêt : 
-	fixer les environnements
-	fixer les versions
-	traçabilité avec fichier de conf yml.
-	problèmes: 
-	channels
-	résolutions de dépendances
-	conflits d'environnements


Singularity
-	intérêt : 
-	portabilité
-	droits utilisateurs (vs. docker)

-	problèmes: 
-	choix de la base
-	versions singularity

Live Tuto
	Construire une image singularity à partir de configurations Conda


## J.2.NextFlow

Présentation
-	intérêt:
-	réutilisabilité
-	optimisation de l'utilisation des ressources de calcul
-	communauté (nf-core)
-	focus:
-	syntaxe
-	DSL2
-	Efficacité/Réutilisabilité/Lisibilité
-	configurations

Live tuto
	Mini pipeline
	FastQC, mapping, comptage
	
  
## J.2.SnakeMake  
  
Snakemake: discussion autour des bonnes pratiques (1h)
- Structuration du workflow
- Configurations (paramétrages, ressources de calcul (profile))
- Modularité (rules et workflows)
- Reporting
- Lisibilité/Traçabilité/Documentation
- Tests
- Portabilité et conteneurisation 
- Optimisation (batch)
- Réutilisation et inspiration : catalogues de workflows

Pas de live tuto

Organisation des groupes de travail : 
- identification des thématiques et délivrables (cf séance visio parallèle snakemake du 30/09)
- constitution des groupes en fonction du niveau des participants
  
Démarrage des travaux  
  
## J.3.Hands On

Introduction / Synthèse:
-	combinaison des 3 technos pour combiner traçabilité, portabilité et calcul

Travail en atelier:
-	des groupes qui travaillent sur leur sujet

Prérequis pour l’atelier :

-	mode --fakeroot sur genotoul vs. VM dédiée avec comptes nominatifs droits sudo
-	[modulo le réseau] VM ou Laptop Linux avec 
    -	singularity
    -	miniconda
    -	JAVA
    -	droits SUDO
    -	build SIF sur Forge ?
-	On prévoit des images de base singularity déjà sur ces clusters au cas ou (salmon, R, …)




