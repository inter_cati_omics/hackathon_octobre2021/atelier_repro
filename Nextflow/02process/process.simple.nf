nextflow.enable.dsl=2

chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)

process gunzip {
	publishDir('result/process.simple')
	tag("${id}") // id correspond a la variable dans input

	input:
	tuple val(id), path(reads)

	output:
	path('*.fastq', emit: outfile)

	script:
    // ici du groovy
    println("[groovy]: fastqID: " + id)
    toto = "titi"
	"""
# ici ce qui me plait, par défaut du bash
#!/usr/bin/env bash
gzip -kcd ${reads[0]} > ${id}.R1.fastq
touch myfile
echo ${toto}
	"""
}

workflow {
	gunzip(chanFastq)
	//gunzip.out[0].view()
	gunzip.out.outfile.view { of -> "[process output] ${of}" }
}
