nextflow.enable.dsl=2


chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)

process gunzip {
	publishDir('result/process.simplepipes')
	tag("${id}") // id correspond a la variable dans input

	input:
	tuple val(id), path(reads)

	output:
	path('*.fastq')

	script:
	println("[groovy][gunzip] fastqID: " + id)
	"""
#!/usr/bin/env bash
gzip -kcd ${reads[0]} > ${id}.R1.fastq
	"""
}

process gzip {
    publishDir('result/process.simplepipes')
    tag("${reads}")

    input:
    path(reads)

    output:
    path('*.fastq.gz')

    script:
    println("[groovy][gzip] file: " + reads)
    """
#!/usr/bin/env bash
gzip -kc ${reads} > ${reads}.gz
    """
}

workflow {
	gunzip(chanFastq) | gzip
	gzip.out[0].view { "[gzip output] " + it}
}
