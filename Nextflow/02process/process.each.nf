nextflow.enable.dsl=2

chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)
num = [0, 1, 2]

process gunzip {
	publishDir('result/process.each')
	tag("${id} ${num}")

	input:
	tuple val(id), path(reads)
	each num

	output:
	path('*.fastq', emit: outfile)

	script:
    println("[groovy] fastqID: " + id + " num: " + num)
	"""
#!/usr/bin/env bash
gzip -kcd $reads[0] > ${id}.R1.${num}.fastq
touch myfile
	"""
}

workflow {
	gunzip(chanFastq, num)
	gunzip.out.outfile.view { of -> "[process output] ${of}" }
}
