nextflow.enable.dsl=2

chanFastq = Channel.fromPath( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', checkIfExists: true)
chanNum = Channel.of(0, 1, 2)

chanFastq.count().view( c -> "file number in channel: ${c}")

process gunzip {
	publishDir('result/process.multichannel')
	tag("${id} ${num}")

	input:
	path(reads)
	val(num)

	output:
	path('*.fastq', emit: outfile)

	script:
    println("[groovy] read: " + reads + " num: " + num)
	"""
#!/usr/bin/env bash
gzip -kcd $reads > output.${num}.fastq
	"""
}

workflow {
	gunzip(chanFastq, chanNum)
	gunzip.out.outfile.view { of -> "[process output] ${of}" }
}
