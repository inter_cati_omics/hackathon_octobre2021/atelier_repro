nextflow.enable.dsl=2

chanFastq = Channel.fromPath( './dataset.csv' ).splitCsv(header: ['sample', 'fileR1', 'fileR2'])

//chanFastq.view { "[channel] csv:$it" }
//chanFastq.map {[it.sample,[it.fileR1, it.fileR2]]}.groupTuple().view()


process mock1 {
	tag("${read1}")

	input:
	tuple val(sample), path(read1), path(read2)

	output:
	tuple val(sample), path('*.sam'), emit: outfile
	tuple val(sample), path('mock.sh'), emit: debug

	script:
	"""
echo "bwa genome ${read1} ${read2} > ${read1}.sam" > mock.sh
touch ${read1}.sam
	"""
}

process mock2 {
	tag("${sample}")

	input:
	tuple val(sample), path(sam)

	output:
	tuple val(sample), path('*merged.sam'), path('mock.sh'), emit: outfile

	script:
	if(sam.collect().size > 1) {
	"""
echo "samtools merge ${sample}.merged.sam ${sam}" > mock.sh
touch ${sample}.merged.sam
	"""
	}
	else {
	"""
echo "cp ${sam} ${sample}.merged.sam" > mock.sh
touch ${sample}.merged.sam
    """
	}
}


workflow {
	mock1(chanFastq)
	mock1.out.debug.view { sample, script -> "[mock1] sample: ${sample} \t script: $script" }
	mock1.out.outfile.view { sample, file -> "[mock1] sample: ${sample} \t file:${file}" }
	mock2(mock1.out.outfile.groupTuple())
    mock2.out.outfile.view { sample, file, script -> "[mock2] sample: ${sample} \t file:${file} \t script: ${script}" }
}
