nextflow.enable.dsl=2

test = Channel.fromPath('./rawdata/dev/control/*.fastq.gz')


process gunzip {
	publishDir('result/operateur.collect2/')

	input:
	val(par)

	output:
	path('collect2.log')

	script:
	println(par.join(' '))
	"""
echo $par > collect2.log
	"""
}

workflow {
	gunzip(test.collect{ "--variant $it " })
	gunzip.out[0].view()
}
