nextflow.enable.dsl=2

chanFastq = Channel.fromPath( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', checkIfExists: true)

process collectme {
	publishDir('result/operateur.collect/')

	input:
	path(reads)

	output:
	path('collect.log', emit: outfile)

	script:
	"""
ls -l *.gz > collect.log
	"""
}

workflow {
	collectme(chanFastq.collect())
	collectme.out.outfile.view()
}
