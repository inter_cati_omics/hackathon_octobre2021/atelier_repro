# Pré requis

## Conda

```bash
wget -q https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh

# yes
# yes

mkdir ~/bin
export PATH=~/miniconda3/bin:$PATH
conda config --set auto_activate_base false
conda config --add channels bioconda conda-forge
```

## Singularity

Ce n'est pas simple car il n'est plus dans les dépôts au moins pour Debian et Ubuntu.

Voici une recette pour Debian/Ubuntu

```bash
apt install libseccomp-dev pkg-config squashfs-tools cryptsetup golang build-essential wget

wget https://github.com/sylabs/singularity/releases/download/v3.8.3/singularity-ce-3.8.3.tar.gz
tar xzf singularity-ce-3.8.3.tar.gz
cd singularity-ce-3.8.3
./mconfig
cd builddir
make
make install
```


## Nextflow

Assurez vous d'avoir un java 8 ou java 11.

```bash
java -version
```

Installation

```bash
curl -s https://get.nextflow.io | bash
```

Vous pouvez déplacer le binaire **nextflow** ou vous voulez.


# Demo Nextflow

## Channel : 01channel

C'est le flux de données utilisé par les process que ce soit un flux provenant des parametres ou d'un autre process.

### Channel en DLS1

En DSL1 il est nécessaire de dupliquer les channels, un channel ne pouvant etre utilisé qu'une fois.

  1. ```nextflow run 01channel/channel.fromFilePairs.dsl1.nf```
  2. essayer de dupliquer la derniere ligne et de relancer le pipeline

### Channel en DSL2

Il y a peu de changements entre le DSL1 et le DSL2 concernant les channels. Le principalement changement c'est qu'un channel peut etre utilisé plusieurs fois sans devoir le dupliquer.

  1. ```nextflow run 01channel/channel.fromPath.nf```
  2. dupliquer la derniere ligne avec `view()` et relancer le pipeline

Tester aussi `01channel/channel.fromFilePairs.nf`, ce channel un peu particulier est très utile en bioinformatique.

## Process

### Process simple

Ce process fait une simple decompression d'un fichier.

```bash
nextflow run 02process/process.simple.nf
```

  - `publishDir` indique où seront les fichiers de sortie.
  - `tag` recupère la variable `id` provenant du bloc `input:` et permet d'avoir le nom du fastq dans les logs pour les tâches du process
  - `println(...)` affiche a l'écran le texte et le contenu de la variable
  - il est possible de créer et d'assigner une variable en groovy et de l'utiliser dans le script bash. Exemple avec la variable `toto`
  - `touch myfile` va créer un fichier mais il n'apparait pas dans le bloc `output` donc il restera dans le répertoire de travail et ne pourra pas etre utilisé par un autre process et ne sera pas disponible dans le `publishDir`

### Process mutli channel

Cet exemple permet de voir comment se comporte un process avec en entrée plusieurs channels. Dans la mesure du possible, c'est bien de l'eviter car ca ajouter de la complexité.

```bash
nextflow run 02process/process.multichannel.nf
```

  - Il y a 6 fichiers en entrée mais nous avons que 3 fichiers en sortie a cause du second channel, `chanNum`, qui n'a que 3 valeurs
  - Quand il y a plusieurs channel dans le bloc `input:`, les channels sont lus de maniere synchrone et le plus petit channel limite le nombre de tâches


### Process avec each

Cet exemple vous montre comment utiliser `each`. Son utilisation est assez spécifique, vous pouvez par exemple lancer plusieurs jobs avec des parametres differents sur le meme jeu de données.

```bash
nextflow run 02process/process.each.nf
```

  - En resultat, une combinatoire entre le channel contenant les fichiers fastq et le channel contenant les chiffres.

### Process avec pipe

Les pipe permettent de simplifier l'écriture mais n'apporte pas d'optimisation en plus. De plus il faut que les sorties du process gunzip correspondent aux parametres en entrée du process gzip.

```bash
nextflow run 02process/process.simplepipes.nf
```

Essayer de remplacer la partie **output** du process **gunzip** par le code en dessous et relancer le pipeline

```
    output:
	tuple val(id), path('*.fastq')
```

## Module

### Module avec un process

Dans le pipeline `module.simple.nf`, nous avons le meme pipeline que precedemment mais avec la creation d'un module pour le process gunzip et un pipeline qui importe ce module pour l'utiliser.

```bash
nextflow run 03workflow_module/module.simple.nf
```

Nous obtenons le meme comportement que pour le pipeline `02process/process.simple.nf`

### Module avec N process

On ajoute un process qui utilise les données du process gunzip.

```bash
 nextflow run 03workflow_module/module.Nprocess.nf
```

A partir d'une bibliotheque de modules, vous poucez composer des workflow assez rapidement.

### Module avec N sub-workflow

Dans ce pipelines, nous avons créer 2 workflows, **gunzipw** et **gzipw** avec un seul process dans chaque worflow. Vous pouvez avoir autant de process que necessaire dans les sub-workflow.

```bash
 nextflow run 03workflow_module/module.Nworkflow.nf
```

  - **gunzipw** va emettre un channel pour le resultat du process gunzip. Il est possible d'emettre plusieurs channel en sortie.
  - **gzipw** ce workflow n'emet pas de channel en sortie (**emit**) donc il n'est pas possible de reutiliser la sortie pour faire autre chose.

Même idée que pour les modules, vous pouvez faire une composition de sub-workflow pour construire un workflow complet.


## Configuration

Le fichier **nextflow.config** au meme niveau que le fichier du pipeline sera chargé automatiquement par Nextflow.

Vous trouverez les résultats dans le répertoire **result/conf_env/**.

La configuration dans le fichier **nextflow.config** active la totalité des rapports et des logs que vous aurez dans **result/conf_env/pipeline_info**.

### Configuration des ressources

Dans cet exemple, le pipeline va charger la configuration du profile **standard** situé dans le fichier **Nextflow.config** et dans le fichier **conf/lipm-smp.config**

```bash
 nextflow run 04configuration_environnement/conf.simple.nf
```

  - le process **gunzip** a un label **multithread** donc il va charger dans le fichier **conf/lipm-smp.config** la partie **withLabel: multithread**
  - le process **gzip** n'a pas de label donc il charge la configuration par défaut du fichier **conf/lipm-smp.config**

Vous pouvez changer de profile avec le parametre `-profile`. Si vous essayez le profile **lipm** ou **genologin** vous aurez une erreur car il va essayer de lancer les jobs sur un cluster SGE ou SLURM qui n'existe pas sur votre machine.

```bash
 nextflow run 04configuration_environnement/conf.simple.nf -profile lipm
```

Vous pouvez changer dans gunzip le label **multithread** par le label **local** et lancer le pipeline. Vous allez voir que les ressources changent.


### Environnement conda et ressources

Exemple d'un pipeline bioinformatique avec conda.

```bash
 nextflow run 04configuration_environnement/conf.conda1.nf --genome $PWD/genome.fasta
```

  - Au premier lancement, il y aura la creation d'un environnement conda, l'indexation et le mapping avec differentes ressources
  - Au second lancement, l'environnement est en cache (comportement par defaut) et l'indexation aussi grace a storeDir


### Environnement et ressources dans des profiles

Dans cet exemple, il faut télécharger (ou créer) l'image singularity et la mettre dans le répertoire **img**. Nous chargeons les profiles **singularity** et **standard** en meme temps.

Allez voir dans le fichier **nextflow.config** comment est mis en place le profile **singularity**. Il y a la possibilité d'utiliser aussi des images docker du dépôt biocontainer. C'est l'exemple commenté.


```bash
cd img
wget https://depot.galaxyproject.org/singularity/bwa%3A0.7.17--h5bf99c6_8
cd ..

nextflow run 04configuration_environnement/conf.conda2.nf  --genome $PWD/genome.fasta  -profile  singularity,standard
```

La meme chose est possible mais avec conda

```bash
nextflow run 04configuration_environnement/conf.conda2.nf  --genome $PWD/genome.fasta  -profile  conda,standard
```

### Environnement dynamique

Vous pouvez faire des benchmarks avec plusieurs versions en utilisant par exemple **conda** et **each**

```bash
nextflow run 04configuration_environnement/conf.env.nf
```

Nextflow va créer 2 environnements avec des versions de samtools différentes et executer le process pour chaque version. Le resultat de cet exemple est un simple fichier de log avec les versions.

  - result/conf_env/software_versions.1.1.txt
  - result/conf_env/software_versions.1.9.txt

## Operateurs

Quelques exemples d'utilisation des operateurs pour traiter des problèmes courants. Il existe beaucoup d'operateur dans le DSL et dans la JVM avec Groovy et Java.

### collect

Cet opérateur va attendre que l'ensemble des fichiers soit disponible avant de lancer la tâche. C'est utile si votre process a besoin de l'ensemble des données.

Un exemple très simple qui vous permet de voir que tous les fichiers du channel sont disponibles dans le répertoire de travail.

```bash
nextflow run 05operateurs/operateur.collect.nf
```

  - dans le fichier `result/05operateurs/collect.log` vous avez le resultat d'un `ls` dans le repertoire de travail. Vous pouvez aussi aller voir dans le répertoire correspondant de `work`.


Dans ce second exemple, le collect va attendre tous les fichiers du channel et les mettre a disposition dans le répertoire de travail mais il y a en plus un traitement pour générer un parametre pour la ligne de commande.

Le traitement est une fonction annonyme qui sera appliquée a tous les elements du channel.

```bash
nextflow run 05operateurs/operateur.collect2.nf
```

  - Dans la console vous allez voir la liste des parametre formée a partir des noms des fichiers
  - Dans le repertoire `result/05operateurs/collect2.log` il y a la ligne avec les parametres sous forme de string

Attention a la taille de la ligne de commande avec cet manière de procéder.

### splitCsv et groupeTuple

Un exemple un peu plus complexe mais plus proche de la réalité. Dans cet exemple nous allons fournir une fichier CSV avec des informations de sample et les fichiers associés. C'est un bon moyen pour prendre en charge les réplicas technique par exemple.

Les CSV fourni n'a pas de header donc nous ajoutons un header avec `header: ['sample', 'fileR1', 'fileR2']`, cela permet d'utiliser le nom des colonnes au lieu de l'index.

```bash
nextflow run 05operateurs/operateur.groupetuple.nf
```

  1. `mock1` va simuler un mapping et les lignes du fichier CSV sont traitées indépendement lors du mapping pour avoir le maximum de parallelisation
  2. `mock2` va simuler la fusion des replicas techniques pour permettre les traitements en aval. C'est groupeTuple() qui va regrouper les fichiers ayant le meme nom de sample (premiere valeur d'un tuple).

Vous pouvez decommenter les lignes avec `view` pour le résultat des opérateurs.

La contrainte majeure de `groupeTuple` c'est qu'il faut un tuple de 2 valeurs seulement. La seconde valeur peut etre une liste.



