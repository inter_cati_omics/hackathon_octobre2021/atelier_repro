nextflow.enable.dsl=2

chanFastq = Channel.fromFilePairs( params.reads, size: 2, checkIfExists: true)

process index {
	storeDir 'indices/bwa'
	
	conda 'bwa'

	input:
	path(genome)
	
	output:
	path("$genome*", emit: indices)
	
	script:
	println("[default] cpus: ${task.cpus}  ;  ${task.memory}")
	"""
bwa index $genome
	"""
}

process mapping {

	publishDir("${params.outdir}/conf.conda1/01.mapping")
	tag("${id}")
	label "multithread"
	
	conda 'bwa'

	input:
	tuple val(id), path(reads)
	path(index)
	path(genome)

	output:
	path('*.sam')

	script:
	println("[mutlithread] mapping: ${task.cpus}  ;  ${task.memory}")
	"""
bwa mem -t ${task.cpus} ${genome} $reads > ${id}.mapping.sam
	"""
}

workflow {
	index(params.genome)
	mapping(chanFastq, index.out.indices, params.genome)
}
