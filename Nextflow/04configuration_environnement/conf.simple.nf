nextflow.enable.dsl=2

chanFastq = Channel.fromFilePairs( params.reads, size: 2, checkIfExists: true) //change

process gunzip {
	publishDir("${params.outdir}/conf.simple/)
	tag("${id}") // id correspond a la variable dans input
	label 'multithread'

	input:
	tuple val(id), path(reads)

	output:
	path('*.fastq')

	script:
    println("[multithread] fastqID: ${id}  ;  cpus: ${task.cpus}  ;  executor: $task.executor")
	"""
# ici ce qui me plait, par défaut du bash
#!/usr/bin/env bash
gzip -kcd ${reads[0]} > ${id}.R1.fastq
touch myfile
	"""
}

process gzip {
    publishDir("${params.outdir}/conf.simple/") // change
    tag("${reads}")

    input:
    path(reads)

    output:
    path('*.fastq.gz')

    script:
    println("[default] fastq: ${reads}  ;  cpus: ${task.cpus}  ;  executor: $task.executor")
    """
#!/usr/bin/env bash
gzip -kc ${reads} > ${reads}.gz
    """
}

workflow {
	gunzip(chanFastq) | gzip
}
