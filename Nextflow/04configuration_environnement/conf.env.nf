nextflow.enable.dsl=2

version = ['1.9','1.1']

process softwareVersions {
	label "local"
	publishDir "${params.outdir}/conf.env/", mode: 'copy'
	
	conda "bwa samtools=${samtoolsVersion}"

	input:
	each(samtoolsVersion)

	output:
	path("software_versions.*.txt")

	script:
	"""
	echo "#Software versions" > software_versions.${samtoolsVersion}.txt
	samtools --version | grep samtools >> software_versions.${samtoolsVersion}.txt
	bwa 2>&1 | grep Version >> software_versions.${samtoolsVersion}.txt
	"""
}

workflow {
	softwareVersions(version)
}
