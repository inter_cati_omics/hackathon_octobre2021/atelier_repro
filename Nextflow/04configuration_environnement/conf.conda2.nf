nextflow.enable.dsl=2

chanFastq = Channel.fromFilePairs( params.reads, size: 2, checkIfExists: true)

process index {
	storeDir 'indices/bwa'
	
	input:
	path(genome)
	
	output:
	path("$genome*", emit: indices)
	
	script:
	println("[default] cpus: ${task.cpus}  ;  ${task.memory}")
	"""
bwa index $genome
	"""
}

process mapping {

	publishDir("${params.outdir}/conf.conda2/01.mapping")
	tag("${id}")
	label "multithread"
		
	input:
	tuple val(id), path(reads)
	path(index)
	path(genome)

	output:
	path('*.sam')
	path('mapping.version')

	script:
	println("[mutlithread] mapping: ${task.cpus}  ;  ${task.memory}")
	"""
bwa 2>&1 | grep -i version > mapping.version
bwa mem -t ${task.cpus} ${genome} $reads > ${id}.mapping.sam
	"""
}

workflow {
	index(params.genome)
	mapping(chanFastq, index.out.indices, params.genome)
}
