nextflow.enable.dsl=2

include { gunzip;gzip } from './module/gzip/main'

chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)

workflow {
	gunzip(chanFastq)
	gzip(gunzip.out.outfile)
}
