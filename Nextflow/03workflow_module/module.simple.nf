nextflow.enable.dsl=2

include { gunzip } from './module/gzip/main.nf'

chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)

workflow {
	gunzip(chanFastq)
	gunzip.out.outfile.view { "[process output] " + it }
}
