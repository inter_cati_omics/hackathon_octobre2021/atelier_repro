nextflow.enable.dsl=2

process gunzip {
	publishDir('result/module/gunzip')
	tag("${id}")

	input:
	tuple val(id), path(reads)

	output:
	path('*.fastq', emit: outfile)

	script:
	println("[module gunzip] fastqID: $id")
	"""
#!/usr/bin/env bash
gzip -kcd ${reads[0]} > ${id}.R1.fastq
touch myfile
	"""
}

process gzip {
    publishDir('result/module/gzip')
    tag("${id}")

    input:
    path(reads)

    output:
    path('*.fastq.gz')

    script:
    println("[module gzip] fastq: $reads")
    """
#!/usr/bin/env bash
gzip -kc ${reads} > ${reads}.gz
    """
}
