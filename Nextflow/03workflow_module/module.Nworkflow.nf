nextflow.enable.dsl=2

include { gunzip;gzip } from './module/gzip/main'

chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)

workflow gunzipw {
	take:
	reads
	
	main:
	gunzip(reads)
	
	emit:
	outfile = gunzip.out.outfile
}


workflow gzipw {
	take:
	reads
	
	main:
	gzip(reads)
}


workflow {
	gunzipw(chanFastq)
	gunzipw.out.outfile.view { "[gunzip sub-workflow output] " + it }

	gzipw(gunzipw.out.outfile)
}
