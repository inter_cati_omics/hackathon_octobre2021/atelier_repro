nextflow.enable.dsl=2

chanFastq = Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true)
chanFastq.view { it -> "[chanFastq] view1: $it" }
chanFastq.view { it -> "[chanFastq] view2: $it" }
