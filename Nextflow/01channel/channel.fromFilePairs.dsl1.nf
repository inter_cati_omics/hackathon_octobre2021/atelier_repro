Channel.fromFilePairs( './rawdata/dev/control/SRR*_{1,2}.fastq.gz', size: 2, checkIfExists: true).into {chanFastq1 ; chanFastq2}
chanFastq1.view { it -> "[chanFastq1] $it" }
chanFastq2.view { it -> "[chanFastq2] $it" }
