# Summary

* [Introduction](README.md)
* [Singularity](Singularity/README.md)  
* [NextFlow](NextFlow/README.md)  
* [Snakemake](Snakemake/README.md) 
  * [bonnes pratiques](Snakemake/best_practices.md) 
