# Hackathon Sète inter-CATI 2021: Atelier reproductibilité snakemake    
![snakemake](img/snakemake.png)

---

Sur cette page vous trouverez les supports à l'atelier snakemake.  

## Gitlab pages  
  
* [gitbook snakemake](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/atelier_repro/Snakemake/)
  
  
## Pré-requis  
  
* Avoir un compte sur genotoul ou une infrastructure de calcul avec slurm/singularity/conda/snakemake de préférence 
* Avoir un terminal pour la connexion ssh  

## Introduction à Snakemake  
  
* [slides](https://slides.com/johanneskoester/snakemake-tutorial)   
  
## [Short Tutorial](https://inter_cati_omics.pages.mia.inra.fr/hackathon_snakemake_2020/short_tutorial/)  
  
## [Snakemake tutorial](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)  
  
## Documentation  
  
* [Site officiel](https://snakemake.github.io/) 
* [Voir l'intro](https://www.youtube.com/watch?v=UOKxta3061g)  
* [Tutoriel vidéo](https://www.youtube.com/watch?v=hPrXcUUp70Y)  
* [Read the docs](https://snakemake.readthedocs.io/)  
* [Read the manuscript](https://doi.org/10.5281/zenodo.4063462)  
* [Snakemake on github](https://github.com/snakemake/snakemake)

## CR de la demie-journée préparatoire du 30/09  
  
```bash
Voici un compte-rendu rapide des propositions sur lesquelles nous nous sommes mis d'accord à la 1/2 journée de préparation au hackathon:

Pour disposer d'un maximum de temps pour travailler ensemble aux problématiques snakemake sur les 2 1/2 journées, on propose:
* que les débutants à snakemake pratiquent snakemake en amont du hackathon (voir les liens vers les éditions précédentes du hackathon reproductibilité)
* une rapide entrée en matière sur les bonne pratiques en snakemake: 1h max

On a identifié 3 problématiques principales que l'on abordera dans l'ordre de complexité:
1. la parallélisation (bedtools en particulier)
2. REPET/singularity (BLASTER, MATCHER, GROUPER from TE_finder in singularity image (https://github.com/urgi-anagen/TE_finder))
3. la récursivité des workflows

Toutes les problématiques intéressant les participants, on propose un groupe de travail unique.
  
On se fixe comme objectif de produire des workflows qui tournent, et de les partager sur la forge comme exemple.
Autre délivrable possible, une cheat sheet qui rassemble les principales commandes, définitions snakemake, accompagnés si possible de courts exemples.
C'est un document qui sera utile pour le déroulement de l'atelier, mais aussi au-delà.
Tout comme les bonnes pratiques, ce sont des supports qui ont vocation à être alimenté de manière collective et qui serviront à la communauté.

D'un point de vue logistique, les dernières informations sur site indiquent que nous avons accès à une salle équipée en wifi avec un débit suffisant.
Néanmoins, Frédérique propose de fournir une VM avec les outils nécessaires pour palier aux problèmes de réseau. 

Au niveau  des pré-requis: une machine linux de préférence, avec un compte sur un cluster disposant au min singularity/conda, snakemake, git + autres binaires en fonction des besoins  
Et dernier point, à la responsabilité des participants d'amener un jeu de données réduit pour permettre de tester les workflows dans un temps raisonnable.

```

## Bonnes pratiques  
  
* voir [https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/atelier_repro/Snakemake/best_practices](https://inter_cati_omics.pages.mia.inra.fr/hackathon_octobre2021/atelier_repro/Snakemake/best_practices)
