# Bonnes pratiques en snakemake

## Pré-requis

* git
* gitlab/github/etc.
* snakemake
* virtualenv/conda 

### options

* docker/singularity (fortement recommandé)

## Dataset de test

Générer un jeu de test (réduit) pour tester le workflow.  

## Snakemake

* mettre son code sous version avec git  
* structure organisée:  
    * exemple: https://snakemake.readthedocs.io/en/stable/snakefiles/deployment.html  
    * see cookiecutter template: https://forgemia.inra.fr/joseph.tran/snakemake-workflow-cookiecutter_template  
    * suivre les usages standards: https://snakemake.github.io/snakemake-workflow-catalog/?rules=true  
* contexte d'exécution:  
    * local  
    * cluster: utilisation des profils (snakemake>=5.10), see https://github.com/Snakemake-Profiles/slurm   
* utilisation d'un fichier de configuration (yaml) pour les données de l'expérience, et utiliser les options en ligne de commande pour les ressources d'exécution:  
    * comme `--set-threads`, `--set-resources`, `--set-default-resources`, et `--directory`
* avant publication:  
    * linter son code (>=5.11) avec `snakemake --lint`  
    * formater son code avec [snakefmt](https://github.com/snakemake/snakefmt)  
    * ajouter des données de test (intégration continue, voir un exemple sur [github](https://github.com/features/actions))  

Optionnel:  
* publication dans des registres:
    * ajouter les métadonnées suivant le standard RO-Crate (https://workflowhub.eu/)

Voir [les bonnes pratiques](https://snakemake.readthedocs.io/en/stable/snakefiles/best_practices.html)   
    
