# BUILD BASIQUE

- base debian
- install de packages via apt
- install d'un outil avec pip

# COMMAND

## BUILD

	singularity build --fakeroot basic.sif Singularity

## HELP

	singularity run-help basic.sif

## TEST
	singularity test basic.sif

## USE

	./basic.sif kallisto
 
