# BUILD AVEC CONDA + SOURCE

- base miniconda
- install de l'environnement de base via conda
- install d'un outil (DISCASM) à partir des sources (via git)

# COMMAND

## BUILD

	singularity build --fakeroot sources.sif Singularity

## HELP

	singularity run-help sources.sif

## TEST
	singularity test sources.sif

## USE

	./sources.sif DISCASM
 
