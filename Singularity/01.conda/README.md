# BUILD AVEC CONDA

- base miniconda
- install de packages de base via apt
- install des outils avec un environnement conda
- fixer une vesion pour un des outils

# COMMAND

## BUILD

	singularity build --fakeroot conda.sif Singularity

## HELP

	singularity run-help conda.sif

## TEST
	singularity test conda.sif

## USE

	./conda.sif kallisto
 
#
# Mode opératoire pour créer un fichier conda yml

 **1. Miniconda installation and configuration**

```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
conda config --set auto_activate_base false
conda deactivate
```
Ajout des channels :
```
conda config --add channels bioconda
conda config --add channels conda-forge
```

**2. Create .yml file**

Creer un environement conda avec les outils necessaires
```
conda create -n test_conda_env -c conda-forge -c bioconda kallisto seqtk
```

Activation de l'environement conda pour pour verifier que les outils sont bien installé
```
source activate test_conda_env
```

Sortir de l'environement conda
```
conda deactivate
```

Export du ficher .yml
```
conda env export -n test_conda_env > test_conda_env.yml
```

Editer le fichier :
- supprimer toutes les dependances non necessaire, 
- enlever le tag
```
cat test_conda_env.yml | cut -d "=" -f 1,2 > test_conda_env.yml
```
- enlever la ligne prefix
- verifier le nom de l'environnement



```
