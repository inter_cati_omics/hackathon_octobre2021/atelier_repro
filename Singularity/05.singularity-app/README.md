# BUILD AVEC PLUSIEURS ENVIRONNEMENTS CONDA ACTIVES A LA DEMANDE

- base miniconda
- install de packages de base via apt
- install des outils avec differents environnements conda (incompatibilités de dépendances)
- utilisation des %app* pour activer à la demande les envionnements 

# COMMAND

## BUILD

	singularity build --fakeroot singularity-app.sif Singularity

## HELP

	singularity run-help singularity-app.sif

## TEST
	singularity test singularity-app.sif

## USE

	singularity run --app kallisto singularity-app.sif kallisto version
	singularity run --app seqtk singularity-app.sif seqtk
 
