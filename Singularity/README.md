# Singularity

{% pdf title="Slides présentation de l'atelier", src="hackaton-Singularity_20211005.pdf", width="100%", height="550", link=false %}{% endpdf %}

Se connecter sur la vm genotoul dédié aux exercices (demander la valeur XX)

    > ssh 147.100.181.XX

Demander le user et le mot de passe ...

Créer un repertoire de travail pour vous :

    > mkdir SebCarrere
    > cd SebCarrere

Cloner le repository 

    > git clone https://hackaton:qiDZDUyhxYHwsFfCoAZz@forgemia.inra.fr/inter_cati_omics/hackathon_octobre2021/atelier_repro.git
    > cd atelier_repro/Singularity/

Aller dans les sous répertoires de singularity pour trouver les exercices.
