# BUILD AVEC PLUSIEURS ENVIRONNEMENTS CONDA

- base miniconda
- install de packages de base via apt
- install des outils avec differents environnements conda (incompatibilités de dépendances)
- activation des environnements 

# COMMAND

## BUILD

	singularity build --fakeroot conda-stack.sif Singularity

## HELP

	singularity run-help conda.sif

## TEST
	singularity test conda.sif

## USE

	./conda.sif kallisto
 
