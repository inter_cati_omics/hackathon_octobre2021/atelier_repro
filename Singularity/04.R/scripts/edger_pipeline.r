#!/usr/bin/env Rscript

libs <- c(
  "reshape2",
  "edgeR",
  "optparse",
  "ggplot2",
  "mixOmics",
  "RColorBrewer",
  "foreach",
  "doParallel"
)

suppressMessages(invisible(lapply(
  lapply(libs, rlang::quo_name),
  library,
  character.only = TRUE
)))

#Execution parallele GLM avec replicats liste de controles vs. liste de groupes

#Controles parametres obligatoires
CheckMandatoryParams <- function (opt, mandatory) {
  for (param in mandatory) {
    if (is.null(opt[[param]])) {
      print_help(opt_parser)
      stop(paste (param, " argument must be supplied", sep = ""), call. = FALSE)
    }
  }
}

#Fonction pour ecrire les logs
Log <- function (msg)
{
  format(Sys.time(), "%a %b %d %X %Y")
  if (opt$verbose) {
    write(paste("[LOG]", Sys.time(), msg, sep = "\t"), stdout())
  }
}
#Fonction clustering + heatmap
PrintHeatMap <- function (data, outfile, colors)
{
  df <- as.matrix(dist(t(data)))
  cim_color <- colorRampPalette(rev(brewer.pal(9, colors)))(32)
  png(filename = paste0(outfile, ".png"), res = 600, width = 4000, height = 4000)
  cim(
    df,
    color = cim_color,
    symkey = FALSE,
    margins = c(10, 10)
  )
  dev.off()
  
}


#Parametres ligne de commande
option_list = list(
  make_option(
    c("-r", "--rawcounts"),
    type = "character",
    help = "rawcount file name",
    metavar = "character"
  ),
  make_option(
    c("-d", "--design"),
    type = "character",
    help = "design file name",
    metavar = "character"
  ),
  make_option(
    c("-c", "--contrasts"),
    type = "character",
    help = "contrasts to perform",
    metavar = "character"
  ),
  make_option(
    c("-o", "--outdir"),
    type = "character",
    default = "./OUTDIR",
    help = "output directory path  [default= %default]",
    metavar = "character"
  ),
  make_option(
    c("-t", "--threads"),
    type = "integer",
    default = 4,
    help = "number of threads [default= %default]",
    metavar = "integer"
  ),
  make_option(
    c("-v", "--verbose"),
    action = "store_true",
    default = FALSE,
    help = "Print little output"
  ),
  make_option(
    c("-V", "--version"),
    action = "store_true",
    default = FALSE,
    help = "Print lib version"
  )
)

opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

if (opt$version) {
  write(capture.output(sessionInfo()), stdout())
  quit()
}
CheckMandatoryParams(opt, c("rawcounts", "design", "contrasts"))


#Controles formats input
#Counts contient une colonne avec les locus_tag (donc un champs dans le header en plus des sample)
columns <-
  scan(text = readLines(opt$rawcounts, 1),
       what = "",
       quiet = TRUE)
if (!startsWith(columns[1], "#")) {
  stop("header must start with '#something' then followed by samples names",
       call. = FALSE)
}
col.names <- columns[-1]

suppressWarnings(
  raw_counts <-
    read.delim2(
      opt$rawcounts,
      header = TRUE,
      row.names = 1,
      col.names = col.names,
      dec = "."
    )
)
design <-
  read.table(opt$design, header = TRUE, stringsAsFactors = FALSE)
if (is.numeric(design$replicate)) {
  stop(
    "'replicate' column in design file contains only numeric values - would be interpreted as quantity and not category - use Rx or Cx or Repx instead",
    call. = FALSE
  )
}

#
# ATTENTION IL Y A UN LIEN FORT ENTRE ORDRE FICHIER DESIGN ET COUNTS !!!!!
# CHECKER ORDRE ? col.names count et $sample de design
#

if (!toString(design$sample) == toString(col.names)) {
  stop ("sample labels column and count sample order differ", call. = FALSE)
}

contrasts <-  read.table(opt$contrasts, header = TRUE)
all_groups <- unique(design$group)

contrasts_strings <- c()
for (line in 1:length(contrasts$test))
{
  test <- contrasts$test[line]
  test_list <- unlist(strsplit(test, ","))
  reference <- contrasts$reference[line]
  if (reference %in% all_groups == FALSE) {
    stop(
      paste(
        "Reference group '",
        reference,
        "' is not in the list of design file groups"
      ),
      call. = FALSE
    )
  }
  
  if (length(test_list) == 1)
  {
    if (test %in% all_groups == FALSE) {
      stop(paste(
        "Test group '",
        test,
        "' is not in the list of design file groups"
      ),
      call. = FALSE)
    }
    contrast_string <- paste(test, "-", reference, sep = "")
  }
  else if (length(test_list) == 2)
  {
    if (test_list[1] %in% all_groups == FALSE) {
      stop(
        paste(
          "Test group '",
          test_list[1],
          "' is not in the list of design file groups"
        ),
        call. = FALSE
      )
    }
    if (test_list[2] %in% all_groups == FALSE) {
      stop(
        paste(
          "Test group '",
          test_list[2],
          "' is not in the list of design file groups"
        ),
        call. = FALSE
      )
    }
    contrast_string <-
      paste("(",
            test_list[1],
            "-",
            reference,
            ")-(",
            test_list[2],
            "-",
            reference,
            ")",
            sep = "")
  }
  else
  {
    stop(
      paste(
        "Cannot deal with more than 2 test groups for each contrast on line ",
        line,
        sep = ""
      ),
      call. = FALSE
    )
  }
  contrasts_strings <- append(contrasts_strings, contrast_string)
}

cpus <- opt$threads
registerDoParallel(cpus)

Log("Create directories")
outdir <- opt$outdir
dir_deg <- paste(outdir, "/deg", sep = "")
dir_img <- paste(outdir, "/images", sep = "")
dir.create(outdir, showWarnings = FALSE)
dir.create(dir_deg, showWarnings = FALSE)
dir.create(dir_img, showWarnings = FALSE)

Log("Load counts, remove null count genes and transform log2")
raw_counts <- as.matrix(raw_counts)
raw_counts_notnull <- raw_counts[rowSums(raw_counts) > 0,]
pseudo_counts <- log2(raw_counts_notnull + 1)
dge <-
  DGEList(raw_counts_notnull, group = colnames(raw_counts_notnull))
#+1 pour eviter les LogFC bidons quand REF = 0
dge$counts = dge$counts + 1
dge$samples$lib.size <- colSums(dge$counts)
### Norm log2_tmm
Log("TMM Normalisation")
dge <- calcNormFactors(dge, method = "TMM")
outfile <- paste(outdir, "/TMM.normFactors.tsv", sep = "")
write.table(
  dge$samples,
  file = outfile,
  sep = "\t",
  row.names = FALSE,
  col.names =  TRUE,
  dec = ",",
  quote = FALSE
)

Log("Write TMM  and Log2 TMM results")
raw_TMM <- cpm(dge)
outfile <- paste(outdir, "/TMM.tsv", sep = "")
write.table(
  raw_TMM,
  file = outfile,
  sep = "\t",
  row.names = TRUE,
  col.names = NA,
  dec = ",",
  quote = FALSE
)

pseudo_TMM <- log2(cpm(dge) + 1)
outfile <- paste(outdir, "/LOG2_TMM.tsv", sep = "")
write.table(
  pseudo_TMM,
  file = outfile,
  sep = "\t",
  row.names = TRUE,
  col.names = NA,
  dec = ",",
  quote = FALSE
)



Log("Pseudo count distribution")
df <- cbind(design, t(pseudo_counts))
df <- melt(df, id.vars = c("sample", "group", "replicate"))
p <- ggplot(data = df, aes(x = sample, y = value, fill = group)) +
  geom_boxplot() + theme_bw() + ggtitle("count distributions") +
  xlab("sample") + ylab("counts") +
  theme(axis.text.x = element_text(angle = 90))
ggsave(
  paste(dir_img, "/pseudocount-boxplots.png", sep = ""),
  plot = last_plot(),
  units = "px",
  width = 3200,
  height = 3200
)


### Reproducibility between samples (MA plots)
Log("Samples similarity based on Log2 TMM distance")
PrintHeatMap(raw_TMM, paste(dir_img, "/TMM-clustering", sep = ""), "Reds")
PrintHeatMap(pseudo_TMM,
             paste(dir_img, "/log2TMM-clustering", sep = ""),
             "Blues")


groups <- as.factor(design$group)
#groups <- factor(paste(design$group))

#0 et construire les contrastes
design_matrix <- model.matrix( ~ 0 + group + replicate, design)
colnames(design_matrix)[1:length(levels(groups))] = levels(groups)


#dge2 <- estimateDisp(dge, design_matrix)

#METHODE FJ
dge <- estimateGLMCommonDisp(dge, design_matrix, verbose = T)
## Estimate dispersion values, relative to the design matrix, using the Cox-Reid (CR)-adjusted likelihood:
dge2 <- estimateGLMTrendedDisp(dge, design_matrix)
dge2 <- estimateGLMTagwiseDisp(dge2, design_matrix)


png(
  filename = paste(dir_img, "/BCV.png", sep = ""),
  width = 3200,
  height = 3200
)
plotBCV(dge2)

fit <- glmFit(dge2, design = design_matrix)



foreach (contrast_id = 1:length(contrasts_strings)) %dopar% {
  contrast_string <- contrasts_strings[contrast_id]
  x <- c(contrast_string)
  my.contrasts = makeContrasts(contrasts = x, levels = design_matrix)
  file_name =  paste(contrast_string,  sep = "")
  Log(paste("Perform GLM ", contrast_string , " as contrast  in parallel", sep = ""))
  res_GLM2 <- glmLRT(fit, contrast = my.contrasts[, 1])
  to_export <-
    topTags(
      res_GLM2,
      n = nrow(res_GLM2$table),
      adjust.method = "BH",
      sort.by = "none"
    )
  outfile <-
    paste(dir_deg , "/" , file_name, ".dge.tsv", sep = "")
  write.table(
    to_export,
    file = outfile,
    row.names = TRUE,
    col.names = NA,
    dec = ",",
    sep = "\t",
    quote = FALSE
  )
  paste("Finished - ", contrast_string, sep = "")
}


writeLines(capture.output(sessionInfo()),
           paste(outdir, "/session.txt", sep = ""))
