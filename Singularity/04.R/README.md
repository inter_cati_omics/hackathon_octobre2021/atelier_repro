# BUILD AVEC RSCRIPT

- base ubuntu + R 4.x.x
- install de packages avec commandes Rscript
- copy script R

# COMMAND

## BUILD

	singularity build --fakeroot R.sif Singularity

## HELP

	singularity run-help R.sif

## TEST
	singularity test R.sif

## USE

	./R.sif edger-pipeline
 
